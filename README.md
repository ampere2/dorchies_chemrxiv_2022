# Dorchies_JACS_144_22734_2022

Contains input files used to perform the simulations of the article:

Controlling the Hydrophilicity of the Electrochemical Interface to Modulate the Oxygen-Atom Transfer in Electrocatalytic Epoxidation Reactions

Florian Dorchies, Alessandra Serva, Dorian Crevel, Jérémy de Freitas, Nikolaos Kostopoulos, Marc Robert, Ozlem Sel, Mathieu Salanne and Alexis Grimaud
*J. Am. Chem. Soc*, 144, 22734 2022

https://pubs.acs.org/doi/10.1021/jacs.2c10764 ([see here for a preprint](https://doi.org/10.26434/chemrxiv-2022-wjsbs-v2))

Each folder contains 2 input files for MetalWalls, which is available [here](https://gitlab.com/ampere2/metalwalls). The corresponding systems are the following:

* Bulk liquids:
	* Acetonitrile - water - LiClO4
	* Acetonitrile - water - LiClO4 with cyclooctene
	* Acetonitrile - water - TBAClO4
	* Acetonitrile - water - TBAClO4 with cyclooctene

* Liquids in contact with gold electrodes:
	* Acetonitrile - water - LiClO4
	* Acetonitrile - water - LiClO4 with cyclooctene
	* Acetonitrile - water - TBAClO4
	* Acetonitrile - water - TBAClO4 with cyclooctene

